import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('index', {
      path: '/'
  });
  this.route('info');
  this.route('knowledge');
  this.route('experience');
  this.route('additionals');
  this.route('finish');
});

export default Router;
