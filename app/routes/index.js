import Ember from 'ember';

export default Ember.Route.extend({
	instance: Ember.inject.service('app-instance'),
	model() {
		this.get('instance').init();
		window.scrollTo(0,0);
		window.localStorage.setItem('currentStep', 'step0');
	}
});
