import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		window.scrollTo(0,0);
		window.localStorage.setItem('currentStep', 'step4');
	}
});
