import Ember from 'ember';

export default Ember.Component.extend({
	isOpen: false,

	instance: Ember.inject.service('app-instance'),

	fromYear: null,
	fromMonth: null,
	from: null,
	to: null,
	toYear: null,
	toMonth: null,
	department: null,
	position: null,
	name: null,
	city: null,
	site: null,
	comments: null,
	language: null,
	timer: null,

	cityList: [
		'Тюмень',
		'Екатеринбург',
		'Москва'
	],
	isTymen: false,
	isEkb: false,
	isMoscow: false,

	willRender() {
		this.set('language', this.get('instance').pop('language'));

		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	attributeBindings: [
		'id',
		'from',
		'to',
		'position',
		'name',
		'city',
		'site'
	],

	actions: {
		citySearch() {
			let tmp = null,
				templateString = null,
				index = 0;
			this.set('isTyumen', false);
			this.set('isEkb', false);
			this.set('isMoscow', false);
			this.get('cityList').forEach((city) => {
				templateString = city.toUpperCase();
				tmp = document.getElementById('city').value.toUpperCase();
				if(templateString.search(tmp) !== -1) {
					if(index === 0) {
						this.set('isTyumen', true);
					}
					if(index === 1) {
						this.set('isEkb', true);
					}
					if(index === 2) {
						this.set('isMoscow', true);
					}
				}
				index +=1;
			});
		},

		chooseCity(city) {
			this.set('isTyumen', false);
			this.set('isEkb', false);
			this.set('isMoscow', false);
			document.getElementById('city').value = city;
		},

		openModal() {
			this.set('isOpen', true);
			document.getElementsByTagName('html')[0].style.overflow = "hidden";
		},
		closeModal() {
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		},
		saveData() {
			let lst = JSON.parse(this.get('instance').pop('knowledge')),
				obj = {
					fromYear: document.getElementById('fromYear').value,
					fromMonth: document.getElementById('fromMonth').value,
					toYear: document.getElementById('toYear').value,
					toMonth: document.getElementById('toMonth').value,
					department: this.get('department'),
					formEdk: document.getElementById('formEdk').value,
					position: this.get('position'),
					name: this.get('name'),
					city: document.getElementById('city').value,
					site: this.get('site'),
					comments: this.get('comments')
				};
			lst.push(obj);
			this.get('instance').push('knowledge', JSON.stringify(lst));
			this.get('instance').push('edList', 'need');
			this.get('instance').push('step2', true);
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		}
	}
});
