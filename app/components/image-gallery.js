import Ember from 'ember';

export default Ember.Component.extend({
	templateNumber: null,
	
	instance: Ember.inject.service('app-instance'),

	attributeBindings: [
	'image'
	],

	language: null,
	timer: null,

	willRender() {
		this.set('templateNumber', this.get('instance').pop('template'));
		this.set('language', this.get('instance').pop('language'));

	    if(this.get('timer') === null) {
	    	this.set('timer', setInterval(() => {
		    	if(this.get('language') !== this.get('instance').pop('language')) {
		    		this.set('language', this.get('instance').pop('language'));
		    	}
		    }, 100));
	    }
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},
	
	actions: {
		chooseOne() {
			this.set('templateNumber', 'one');
			this.get('instance').push('template', 'one');
		},
		chooseTwo() {
			this.set('templateNumber', 'two');
			this.get('instance').push('template', 'two');
		},
		chooseThree() {
			this.set('templateNumber', 'three');
			this.get('instance').push('template', 'three');
		}
	}
});
