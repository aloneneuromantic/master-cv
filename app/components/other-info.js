import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	otherInfo: null,
	language: null,
	template: null,
	timer: null,
	languages: null,
	languagesString: null,
	step: null,

	init() {
		this._super(...arguments);
		this.set('languages', JSON.parse(window.localStorage.getItem('other')).otherLanguages);
		let count = 1,
			max = this.get('languages').length,
			str ='';
		this.get('languages').forEach(item =>{
			str = str + `${item.name}`;

			if (item.state === '1') {
				str = str + ' (базовый)';
			}
			if (item.state === '2') {
				str = str + ' (чтение проф. литературы)';
			}
			if (item.state === '3') {
				str = str + ' (свободный)';
			} 
			if (item.state === '4') {
				str = str + ' (технической)';
			}


			if(count === max) {
				str = str + '.';
				this.set('languagesString', str);
				this.get('instance').push('languageRefresh', 'none');
			} else {
				count += 1;
				str = str + ', ';
			}
		});
	},

	willRender() {
		this.set('otherInfo', JSON.parse(this.get('instance').pop('other')));
		this.set('language', this.get('instance').pop('language'));
		this.set('template', this.get('instance').pop('template'));
		this.set('step', this.get('instance').pop('currentStep'));

		this.set('languages', JSON.parse(window.localStorage.getItem('other')).otherLanguages);
		let count = 1,
			max = this.get('languages').length,
			str ='';
		this.get('languages').forEach(item =>{
			str = str + `${item.name}`;

			if (item.state === '1') {
				str = str + ' (базовый)';
			}
			if (item.state === '2') {
				str = str + ' (чтение проф. литературы)';
			}
			if (item.state === '3') {
				str = str + ' (свободный)';
			} 
			if (item.state === '4') {
				str = str + ' (технической)';
			}


			if(count === max) {
				str = str + '.';
				this.set('languagesString', str);
				this.get('instance').push('languageRefresh', 'none');
			} else {
				count += 1;
				str = str + ', ';
			}
		});			
		
		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('instance').pop('needRefresh') === 'need') {
					this.set('otherInfo', JSON.parse(this.get('instance').pop('other')));
					this.get('instance').push('needRefresh', 'none');
				}
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));				
				}
				if(this.get('template') !== this.get('instance').pop('template')) {
					this.set('template', this.get('instance').pop('template'));
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	}

});
