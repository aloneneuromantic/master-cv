import Ember from 'ember';

export default Ember.Component.extend({
	iSeeAPanel: null,

	init() {
		this._super(...arguments);
		this.set('iSeeAPanel', false);
		console.log(this.get('iSeeAPanel'));
	},

	mouseEnter: function() {
	    this.set('iSeeAPanel', true);
	},
	mouseLeave: function() {
	    this.set('iSeeAPanel', false);
	},

});
