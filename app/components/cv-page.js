import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	fio: null,
	position: null,
	wage: null,
	phone: null,
	skype: null,
	email: null,
	site: null,
	dateOfBirth: null,
	language: null,
	timer: null,
	city: null,
	gender: null,
	citizenship: null,
	pereezd: null,
	komandirovki: null,
	attributeBindings: [
		'state'
	],

	willRender() {
		[
		'pereezd',
		'komandirovki',
		'city',
		'gender',
		'citizenship',
	    'site',
	    'fio',
	    'position',
	    'template',
	    'wage',
	    'phone',
	    'skype',
	    'email',
	    'dateOfBirth'].forEach((item) => {
	    	this.set(item, this.get('instance').pop(item));
	    });

	    this.set('language', this.get('instance').pop('language'));

	    if(this.get('timer') === null) {
	    	this.set('timer', setInterval(() => {
		    	if(this.get('instance').pop('needRefresh') === 'need') {
		    		this.set('language', this.get('instance').pop('language'));
		    		this.get('instance').push('needRefresh', 'not-need');
		    	}
		    }, 100));
	    }
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		finalState() {
			this.get('instance').push('currentStep', null);
		}
	}
});
