import Ember from 'ember';

export default Ember.Component.extend({
    isOpen: false,

    instance: Ember.inject.service('app-instance'),

    language: null,
    timer: null,

    willRender() {
        this.set('language', this.get('instance').pop('language'));

        if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));
				}
			}, 100));
		}
    },

    willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

    actions: {
        toggleModal() {
            if(this.get('isOpen') === true) {
                this.set('isOpen', false);
				document.getElementsByTagName('html')[0].style.overflow = "";
            } else {
                this.set('isOpen', true);
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
            }

        },
        save() {
            let lst = JSON.parse(this.get('instance').pop('additionalKnowledge')),
                obj = {
                    description: document.getElementById('description-add').value,
					type: (document.getElementById('type-0').checked === true) ? 'сертефикат' : 'свидетельство',
					name: document.getElementById('name-add').value,
				    dateYear: document.getElementById('dateYear-add').value,
					dateMonth: document.getElementById('dateMonth-add').value
                };
            lst.push(obj);
			this.get('instance').push('additionalKnowledge', JSON.stringify(lst));
			this.get('instance').push('addEdList', 'need');
            this.get('instance').push('edList', 'need');
			this.get('instance').push('step2', true);
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
        }
    }
});
