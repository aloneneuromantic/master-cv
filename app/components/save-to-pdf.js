import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	language: null,
	timer: null,
	isOpen: false,

	willRender() {
		this.set('language', this.get('instance').pop('language'));

		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));
				}			
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		savePdf() { 
			console.log(this.get('instance').getData());
			this.set('isOpen', true);
		},
		saveDoc() {
			console.log(this.get('instance').getData());
			this.set('isOpen', true);
		},
		print() {
			console.log(this.get('instance').getData());
			this.set('isOpen', true);
		},
		emailSend() {
			console.log(this.get('instance').getData());	
			this.set('isOpen', true);
		},
		closeModal() {
			this.set('isOpen', false);	
		}
	}
});
