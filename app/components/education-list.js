import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	educationList: null,
	template: null,
	language: null,
	timer: null,
	currentStep: null,

	willRender() {
		this.set('educationList', JSON.parse(this.get('instance').pop('knowledge')));

		console.log(this.get('educationList'));

		this.set('language', this.get('instance').pop('language'));
		this.set('template', this.get('instance').pop('template'));
		this.set('currentStep', this.get('instance').pop('currentStep'));
		 if(this.get('timer') === null) {
		 	this.set('timer', setInterval(() => {
		 		if(this.get('instance').pop('edList') === 'need') {
		 			this.set('educationList', JSON.parse(this.get('instance').pop('knowledge')));
		 			this.get('instance').push('edList', 'none');
		 		}
		 		if(this.get('language') !== this.get('instance').pop('language')) {
		 			this.set('language', this.get('instance').pop('language'));
		 		}
		 		if(this.get('template') !== this.get('instance').pop('template')) {
		 			this.set('template', this.get('instance').pop('template'));
		 		}
		 	}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	}
});
