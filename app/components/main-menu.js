import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	isOpen: false,

	language: null,
	currentStep: null,
	timer: null,

	willRender() {
		this.set('language', this.get('instance').pop('language'));
		this.set('currentStep', this.get('instance').pop('currentStep'));
		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('currentStep') !== this.get('instance').pop('currentStep')) {
					this.set('currentStep', this.get('instance').pop('currentStep'));
				}
			}, 100));
		}
	},

	attributeBindings: [
		'state'
	],
	
	actions: {
		setStep1() {
			this.set('currentStep', 'step1');
			this.get('instance').push('currentStep', 'step1');
			this.set('isOpen', false);
		},
		setStep2() {
			this.set('currentStep', 'step2');
			this.get('instance').push('currentStep', 'step2');
			this.set('isOpen', false);
		},
		setStep3() {
			this.set('currentStep', 'step3');
			this.get('instance').push('currentStep', 'step3');
			this.set('isOpen', false);
		},
		setStep4() {
			this.set('currentStep', 'step4');
			this.get('instance').push('currentStep', 'step4');
			this.set('isOpen', false);
		},
		setFinish() {
			this.set('currentStep', 'finish');
			this.get('instance').push('currentStep', 'finish');
			this.set('isOpen', false);
		},
		toggleMenu() {
			if(this.get('isOpen') === false) {
				this.set('isOpen', true);
				document.onmousewheel = document.onwheel = function() { 
					return false;
				};
				document.addEventListener("MozMousePixelScroll", function() {
					return false;
				}, false);
				document.onkeydown=function(e) {
					if (e.keyCode>=33&&e.keyCode<=40) { 
						return false;
					}
				}
			} else {
				this.set('isOpen', false);
				document.onmousewheel = document.onwheel = function() { 
					return true;
				};
				document.addEventListener("MozMousePixelScroll", function() {
					return true;
				}, true);
				document.onkeydown=function(e) {
					if (e.keyCode>=33&&e.keyCode<=40) {
						return true;
					}
				}
			}
		},
		setRussian() {
			this.get('instance').push('language', 'ru');
			this.set('language', 'ru');
			this.get('instance').push('needRefresh', 'need');
		},
		setEnglish() {
			this.get('instance').push('language', 'eng');
			this.set('language', 'eng');
			this.get('instance').push('needRefresh', 'need');
		}

	}
});
