import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'span',
	classNames: ['icons'],
	attributeBindings: [
		'title',
		'text'
	],
	isOpen: false,

	actions: {
		openModal() {
			this.set('isOpen', true);
		},
		closeModal() {
			this.set('isOpen', false);
		}
	}
});
