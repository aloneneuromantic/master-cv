import Ember from 'ember';

export default Ember.Component.extend({

	instance: Ember.inject.service('app-instance'),

	modalState: null,
	language: null,
	template: null,
	timer: null,
	step: null,

	attributeBindings: [
		'fromMonth',
		'fromYear',
		'toMonth',
		'toYear',
		'position',
		'pride',
		'name',
		'sphere',
		'city',
		'site',
		'jobs',
		'achivki'
	],

	willRender() {
		this.set('language', this.get('instance').pop('language'));
		this.set('template', this.get('instance').pop('template'));
		this.set('step', this.get('instance').pop('currentStep'));


		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));
				}
				if(this.get('template') !== this.get('instance').pop('template')) {
					this.set('template', this.get('instance').pop('template'));
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		deleteWork() {
			let lst = JSON.parse(this.get('instance').pop('work')),
				tmp = [],
				max = lst.length,
				iterator = 1;
			lst.forEach((place) => {
				if(place.name !== this.get('name')) {
					tmp.push(place);
				}

				if(iterator === max) {
					this.get('instance').push('work', JSON.stringify(tmp));
					window.localStorage.setItem('wrkList', 'need');
				} else {
					iterator += 1;
				}
			});
			document.getElementById(`trashModal-${this.get('name')}`).classList.remove('is-active');
		},
		openEdit() {
			document.getElementById(`editModal-${this.get('name')}`).classList.add('is-active');
			document.getElementsByTagName('html')[0].style.overflow = "hidden";
			let tmp = '',
				lst = JSON.parse(window.localStorage.getItem('work'))[0].achivki.split(','),
				max = lst.length,
				iterator = 1;
			lst.forEach(item => {
				if(iterator === max) {
					tmp = tmp + item;
					document.getElementById(`achivki-${this.get('name')}`).value = tmp;
				} else {
					tmp = tmp + item + '\n';
					iterator += 1;
				}
			});
			let tmpJ = '',
				lstJ = JSON.parse(window.localStorage.getItem('work'))[0].jobs.split('\n'),
				maxJ = lstJ.length,
				iteratorJ = 1;
			lstJ.forEach(itemJ => {
				if(iteratorJ === maxJ) {
					tmpJ = tmpJ + itemJ;
					document.getElementById(`jobs-${this.get('name')}`).value = tmpJ;
				} else {
					tmpJ = tmpJ + itemJ + '\n';
					iteratorJ = iteratorJ + 1;
				}
			});
		},
		saveEdit() {
			let tmp = JSON.parse(this.get('instance').pop('work'));
			tmp.forEach((item) => {
				if(item.name === this.get('name')) {
					item.fromMonth = document.getElementById(`fromMonth-${this.get('name')}`).value;
					item.fromYear = document.getElementById(`fromYear-${this.get('name')}`).value;
					item.toMonth = document.getElementById(`toMonth-${this.get('name')}`).value;
					item.toYear = document.getElementById(`toYear-${this.get('name')}`).value;
					item.position = document.getElementById(`position-${this.get('name')}`).value;
					item.pride = document.getElementById(`pride-${this.get('name')}`).value;
					item.name = document.getElementById(`name-${this.get('name')}`).value;
					item.sphere = document.getElementById(`sphere-${this.get('name')}`).value;
					item.city = document.getElementById(`city-${this.get('name')}`).value;
					item.site = document.getElementById(`site-${this.get('name')}`).value;
					item.jobs = document.getElementById(`jobs-${this.get('name')}`).value;
					item.achivki = document.getElementById(`achivki-${this.get('name')}`).value;
					this.get('instance').push('work', JSON.stringify(tmp));
					this.set('fromMonth', item.fromMonth);
					this.set('fromYear', item.fromYear);
					this.set('toMonth', item.toMonth);
					this.set('toYear', item.toYear);
					this.set('position', item.position);
					this.set('pride', item.pride);
					this.set('name', item.name);
					this.set('sphere', item.sphere);
					this.set('city', item.city);
					this.set('site', item.site);
					this.set('jobs', item.jobs);
					this.set('achivki', item.achivki);
					document.getElementById(`editModal-${this.get('name')}`).classList.remove('is-active');
					document.getElementsByTagName('html')[0].style.overflow = "";
				}
			});
		},
		closeEdit() {
			document.getElementById(`editModal-${this.get('name')}`).classList.remove('is-active');
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		},
		openTrash() {
			document.getElementById(`trashModal-${this.get('name')}`).classList.add('is-active');
		},
		closeTrash() {
			document.getElementById(`trashModal-${this.get('name')}`).classList.remove('is-active');
		}
	}
});
