import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),
	isOpen: false,
	language: null,
	timer: null,

	attributeBindings: [
		'ruTitle',
		'engTitle',
		'ruDescription',
		'engDescription'
	],

	willRender() {
		this.set('language', this.get('instance').pop('language'));
		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));				
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		openModal() {
			this.set('isOpen', true);
		},
		closeModal() {
			this.set('isOpen', false);
		}
	}
});
