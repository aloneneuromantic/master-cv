import Ember from 'ember';

export default Ember.Component.extend({
    instance: Ember.inject.service('app-instance'),

	addtitionList: null,
	timer: null,

	willRender() {
		this.set('addtitionList', JSON.parse(this.get('instance').pop('additionalKnowledge')));
		if(this.get('timer') === null) {
		 	this.set('timer', setInterval(() => {
		 		if(this.get('instance').pop('addEdList') === 'need') {
					this.set('addtitionList', JSON.parse(this.get('instance').pop('additionalKnowledge')));
		 			this.get('instance').push('addEdList', 'none');
		 		}
		 	}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	}
});
