import Ember from 'ember';

export default Ember.Component.extend({

	instance: Ember.inject.service('app-instance'),

	modalState: null,
	language: null,
	template: null,
	timer: null,
	step: null,

	cityList: [
		'Тюмень',
		'Екатеринбург',
		'Москва'
	],

	attributeBindings: [
		'fromYear',
		'fromMonth',
	    'toYear',
	    'toMonth',
	   	'department',
	    'formEdk',
		'position',
		'name',
		'city',
		'site',
		'comments'],

	willRender() {
		this.set('language', this.get('instance').pop('language'));
		this.set('template', this.get('instance').pop('template'));
		this.set('step', this.get('instance').pop('currentStep'));
		this.set('timer', setInterval(() => {
			if(this.get('language') !== this.get('instance').pop('language')) {
				this.set('language', this.get('instance').pop('language'));
			}
			if(this.get('template') !== this.get('instance').pop('template')) {
				this.set('template', this.get('instance').pop('template'));
			}
		}, 100));
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		citySearchEdit() {
			let tmp = null,
				templateString = null,
				index = 0,
				isTymen = false,
				isEkb = false,
				isMoscow = false;
			
			if(document.getElementById(`isTyumen-${this.get('name')}`)) {
				document.getElementById('city-p').removeChild(document.getElementById(`isTyumen-${this.get('name')}`));
			}

			if(document.getElementById(`isEkb-${this.get('name')}`)) {
				document.getElementById('city-p').removeChild(document.getElementById(`isEkb-${this.get('name')}`));
			}

			if(document.getElementById(`isMoscow-${this.get('name')}`)) {
				document.getElementById('city-p').removeChild(document.getElementById(`isMoscow-${this.get('name')}`));
			}

			this.get('cityList').forEach((city) => {
				templateString = city.toUpperCase();
				tmp = document.getElementById(`city-${this.get('name')}`).value.toUpperCase();
				if(templateString.search(tmp) !== -1) {
					if(index === 0) {
						let obj = document.createElement('a');
						obj.className = obj.className + 'button is-bizzare-pure-button is-fullwidth ';
						obj.text = "Тюмень";
						obj.id = `isTyumen-${this.get('name')}`;
						obj.onclick = () => {
							document.getElementById(`city-${this.get('name')}`).value = 'Тюмень';
							document.getElementById('city-p').removeChild(document.getElementById(`isTyumen-${this.get('name')}`));
						};
						document.getElementById('city-p').appendChild(obj);
					}
					if(index === 1) {
						let obj = document.createElement('a');
						obj.className = obj.className + 'button is-bizzare-pure-button is-fullwidth ';
						obj.text = "Екатеринбург";
						obj.id = `isEkb-${this.get('name')}`;
						obj.onclick = () => {
							document.getElementById(`city-${this.get('name')}`).value = 'Екатеринбург';
							document.getElementById('city-p').removeChild(document.getElementById(`isEkb-${this.get('name')}`));
						};
						document.getElementById('city-p').appendChild(obj);
					}
					if(index === 2) {
						let obj = document.createElement('a');
						obj.className = obj.className + 'button is-bizzare-pure-button is-fullwidth ';
						obj.text = "Москва";
						obj.id = `isMoscow-${this.get('name')}`;
						obj.onclick = () => {
							document.getElementById(`city-${this.get('name')}`).value = 'Москва';
							document.getElementById('city-p').removeChild(document.getElementById(`isMoscow-${this.get('name')}`));
						};
						document.getElementById('city-p').appendChild(obj);
					}
				}
				index +=1;
			});
		},

		deleteWork() {
			let lst = JSON.parse(this.get('instance').pop('knowledge')),
				tmp = [],
				max = lst.length,
				iterator = 1;
			lst.forEach((place) => {
				if(place.position !== this.get('position')) {
					tmp.push(place);
				}

				if(iterator === max) {
					this.get('instance').push('knowledge', JSON.stringify(tmp));
					this.get('instance').push('needRefresh', 'need');
					this.get('instance').push('edList', 'need');
				} else {
					iterator += 1;
				}
			});
		},
		openEdit() {
			document.getElementById(`editModal-${this.get('name')}`).classList.add('is-active');
			document.getElementsByTagName('html')[0].style.overflow = "hidden";
		},
		saveEdit() {
			let tmp = JSON.parse(this.get('instance').pop('knowledge'));
			tmp.forEach((item) => {
				if(item.id === this.get('id')) {
					item.position = document.getElementById(`position-${this.get('name')}`).value;
					item.fromYear = document.getElementById(`fromYear-${this.get('name')}`).value;
					item.fromMonth = document.getElementById(`fromMonth-${this.get('name')}`).value;
					item.toYear = document.getElementById(`toYear-${this.get('name')}`).value;
					item.toMonth = document.getElementById(`toMonth-${this.get('name')}`).value;
					item.department = document.getElementById(`department-${this.get('name')}`).value;
					item.formEdk = document.getElementById(`formEdk-${this.get('name')}`).value;
					item.comments = document.getElementById(`comments-${this.get('name')}`).value;
					item.site = document.getElementById(`site-${this.get('name')}`).value;
					item.name = document.getElementById(`name-${this.get('name')}`).value;
					item.city = document.getElementById(`city-${this.get('name')}`).value;
					this.get('instance').push('knowledge', JSON.stringify(tmp));
					this.set('position', item.position);
					this.set('comments', item.comments);
					this.set('city', item.city);
					this.set('name', item.name);
					this.set('site', item.site);
					this.set('department', item.department);
					this.set('fromYear', item.formYear);
					this.set('fromMonth', item.fromMonth);
					this.set('toYear', item.toYear);
					this.set('toMonth', item.toMonth);
					document.getElementById(`editModal-${this.get('name')}`).classList.remove('is-active');
					document.getElementsByTagName('html')[0].style.overflow = "";
				}
			});
		},
		closeEdit() {
			document.getElementById(`editModal-${this.get('name')}`).classList.remove('is-active');
			document.getElementsByTagName('html')[0].style.overflow = "";
		},
		openTrash() {
			document.getElementById(`trashModal-${this.get('name')}`).classList.add('is-active');
		},
		closeTrash() {
			document.getElementById(`trashModal-${this.get('name')}`).classList.remove('is-active');
		}
	}
});
