import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	isOpen: false,

	otherInfo: null,
	step1: null,
	step2: null,
	step3: null,
	step4: null,
	language: null,
	timer: null,
	myLanguages: 'Языки не выбраны',
	languageList: [
		'русский',
		'английский'
	],
	isRussian: false,
	isEnglish: false,
	thisLanguage: null,

	isDriverNo: false,
	isDriverA: false,
	isDriverB: false,
	isDriverC: false,
	isDriverD: false,
	isDriverE: false,
	isSave: false,
	isEdit: false,
	ifList: false,
	editedName: null,
	afterDelete: null,
	isEditRemove: false,
	pleaseCleanMe: false,
	isAdded: false,
	iSeeABlue: false,

	init() {
		this._super(...arguments);
		let tmp = JSON.parse(window.localStorage.getItem('other')).driverLicense;
		if(tmp.indexOf('Нет') !== -1) {
			this.set('isDriverNo', true);
			this.set('isDriverA', false);
			this.set('isDriverB', false);
			this.set('isDriverC', false);
			this.set('isDriverD', false);
			this.set('isDriverE', false);
		}
		if(tmp.indexOf('A') !== -1) {
			this.set('isDriverA', true);
		}
		if(tmp.indexOf('B') !== -1) {
			this.set('isDriverB', true);
		}
		if(tmp.indexOf('C') !== -1) {
			this.set('isDriverC', true);
		}
		if(tmp.indexOf('D') !== -1) {
			this.set('isDriverD', true);
		}
		if(tmp.indexOf('E') !== -1) {
			this.set('isDriverE', true);
		}

		if(JSON.parse(window.localStorage.getItem('other')).save === 'yes') {
			this.set('isSave', true);
		}

		this.set('myLanguages', JSON.parse(window.localStorage.getItem('other')).otherLanguages);
	},

	willRender() {

		this.set('otherInfo', JSON.parse(this.get('instance').pop('other')));
		this.set('isFinal', JSON.parse(this.get('instance').pop('step1')));
		this.set('isFinal', JSON.parse(this.get('instance').pop('step2')));
		this.set('isFinal', JSON.parse(this.get('instance').pop('step3')));
		this.set('isFinal', JSON.parse(this.get('instance').pop('step4')));
		this.set('language', this.get('instance').pop('language'));

		if(this.get('timer') == null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));
				}
				if(this.get('isAdded') === false) {
					this.set('isAdded', true);
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		// WORK THIS
		deleteLanguage(name) {
			document.getElementById('myLanguages').removeChild(document.getElementById(`lang-${name}`));
			let listOfLanguages = this.get('myLanguages'),
				newList = [];
			listOfLanguages.forEach(item => {
				if(item.name !== name) {
					newList.push(item);
				}
			});
			this.set('myLanguages', newList);
		},
		// WORK THIS
		editLanguage(name, state) {
			document.getElementById('myLanguages').removeChild(document.getElementById(`lang-${name}`));
			let listOfLanguages = this.get('myLanguages'),
				newList = [];
			listOfLanguages.forEach(item => {
				if(item.name !== name) {
					newList.push(item);
				}
			});
			this.set('myLanguages', newList);
			document.getElementById('otherLanguages').value = name;
			document.getElementById('languageState').value = state;
		},
		// WORK THIS
		languageSearch() {
			let tmp = null,
				templateString = null,
				index = 0;
			this.set('isRussian', false);
			this.set('isEnglish', false);
			this.set('thisLanguage', null);
			this.get('languageList').forEach((language) => {
				templateString = language.toUpperCase();
				tmp = document.getElementById('otherLanguages').value.toUpperCase();
				this.set('thisLanguage', document.getElementById('otherLanguages').value);
				if(templateString.search(tmp) !== -1) {
					if(index === 0) {
						this.set('isRussian', true);
						this.set('thisLanguage', null);
						this.set('iSeeABlue', true);
					}
					if(index === 1) {
						this.set('isEnglish', true);
						this.set('thisLanguage', null);
						this.set('iSeeABlue', true);
					}
				}
				index +=1;
			});
		},

		saveOrAddLanguage() {
			let tmp = {
			 	name: document.getElementById('otherLanguages').value,
			 	state: document.getElementById('languageState').value
			};
			this.get('myLanguages').push(tmp);
			this.set('isAdded', false);
		},

		chooseLanguage(language) {
			document.getElementById('otherLanguages').value = language;
			this.set('iSeeABlue', false);
		},
		driverNo() {
			if(this.get('isDriverNo') === false) {
				this.set('isDriverA', false);
				this.set('isDriverB', false);
				this.set('isDriverC', false);
				this.set('isDriverD', false);
				this.set('isDriverE', false);
				this.set('isDriverNo', true);
			} else {
				this.set('isDriverNo', false);
			}
		},
		driverA() {
			this.set('isDriverNo', false);
			if(this.get('isDriverA') === true) {
				this.set('isDriverA', false);
			} else {
				this.set('isDriverA', true);
				this.set('isDriverNo', false);
			}
		},
		driverB() {
			this.set('isDriverNo', false);
			if(this.get('isDriverB') === true) {
				this.set('isDriverB', false);
			} else {
				this.set('isDriverB', true);
				this.set('isDriverNo', false);
			}
		},
		driverC() {
			this.set('isDriverNo', false);
			if(this.get('isDriverC') === true) {
				this.set('isDriverC', false);
			} else {
				this.set('isDriverC', true);
				this.set('isDriverNo', false);
			}
		},
		driverD() {
			this.set('isDriverNo', false);
			if(this.get('isDriverD') === true) {
				this.set('isDriverD', false);
			} else {
				this.set('isDriverD', true);
				this.set('isDriverNo', false);
			}
		},
		driverE() {
			this.set('isDriverNo', false);
			if(this.get('isDriverE') === true) {
				this.set('isDriverE', false);
			} else {
				this.set('isDriverE', true);
				this.set('isDriverNo', false);
			}
		},
		toggleSave() {
			if(this.get('isSave') === false) {
				this.set('isSave', true);
			} else {
				this.set('isSave', false);
			}
		},
		saveChanges() {
			let tmp = JSON.parse(this.get('instance').pop('other'));
			tmp.otherLanguages = this.get('myLanguages');
			tmp.computer = document.getElementById('computer').value;
			tmp.personalQualities = document.getElementById('personalQualities').value;
			tmp.additionalInfo = document.getElementById('additionalInfo').value;
			tmp.prichina = document.getElementById('prichina').value;
			tmp.profskils = document.getElementById('profskils').value;
			tmp.save = (this.get('isSave') === true) ? 'yes' : 'no';
			tmp.driverLicense ='';
			if(this.get('isDriverNo') === true) {
				tmp.driverLicense = 'Нет';
			} else {
				if(this.get('isDriverA') === true) {
					tmp.driverLicense = tmp.driverLicense + 'A';
				}
				if(this.get('isDriverB') === true) {
					tmp.driverLicense = tmp.driverLicense + 'B';
				}
				if(this.get('isDriverC') === true) {
					tmp.driverLicense = tmp.driverLicense + 'C';
				}
				if(this.get('isDriverD') === true) {
					tmp.driverLicense = tmp.driverLicense + 'D';
				}
				if(this.get('isDriverE') === true) {
					tmp.driverLicense = tmp.driverLicense + 'E';
				}
			}
		    this.get('instance').push('other', JSON.stringify(tmp));
			this.get('instance').push('needRefresh', 'need');
			this.get('instance').push('step4', true);
			this.set('step4', true);
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		},
		toggleModal() {
			if(this.get('isOpen') === false) {
				this.set('isOpen', true);
				this.set('isAdded', true);
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
			} else {
				this.set('isOpen', false);
				document.getElementsByTagName('html')[0].style.overflow = "";
			}
		}
	}
});
