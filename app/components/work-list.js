import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	workList: null,
	language: null,
	template: null,
	timer: null,
	currentStep: null,

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	willRender() {
		let tmp = JSON.parse(this.get('instance').pop('work')),
				max = tmp.length,
				iterator = 1,
				arr = null;
			tmp.forEach((item) => {
				if (item.jobs !== null) {
					arr = item.jobs.split('\n');
					tmp[iterator-1].jobs = arr;
				} else {
					tmp[iterator-1].jobs = ['Не заполнено'];
				}

				if (item.achivki !== null) {
					arr = item.achivki.split('\n');
					tmp[iterator-1].achivki = arr;
				} else {
					tmp[iterator-1].achivki = ['Не заполнено'];
				}

				if(iterator === max) {
					this.set('workList', tmp);
				} else {
					iterator += 1;
				}
			});



		this.set('language', this.get('instance').pop('language'));
		this.set('template', this.get('instance').pop('template'));
		this.set('currentStep', this.get('instance').pop('currentStep'));
		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {

				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));
				}

				if(this.get('template') !== this.get('instance').pop('template')) {
					this.set('template', this.get('instance').pop('template'));
				}

				if(this.get('instance').pop('wrkList') === 'need') {
					let tmp = JSON.parse(this.get('instance').pop('work')),
						max = tmp.length,
						iterator = 1,
						arr = null;
					if(max === 0) {
						this.set('workList', tmp);
						this.get('instance').push('wrkList', 'none');
					} else {
						tmp.forEach((item) => {
								if (item.jobs !== null) {
									arr = item.jobs.split(',');
									tmp[iterator-1].jobs = arr;
								} else {
									tmp[iterator-1].jobs = ['Не заполнено'];
								}

								if(iterator === max) {
									this.set('workList', tmp);
									this.get('instance').push('wrkList', 'none');
								} else {
									iterator += 1;
								}
							});
					}
				}
			}, 100));
		}
	}
});
