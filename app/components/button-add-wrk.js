import Ember from 'ember';

export default Ember.Component.extend({
	isOpen: false,

	instance: Ember.inject.service('app-instance'),

	fromMonth: null,
	fromYear: null,
	toMonth: null,
	toYear: null,
	position: null,
	pride: null,
	name: null,
	sphere: null,
	city: null,
	site: null,
	jobs: null,
	achivki: null,
	language: null,


	willRender() {
		this.set('language', this.get('instance').pop('language'));

		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));				
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		openModal() {
			this.set('isOpen', true);
			document.getElementsByTagName('html')[0].style.overflow = "hidden";
		},
		closeModal() {
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		},
		saveData() {
			let lst = JSON.parse(this.get('instance').pop('work')),
			 	obj = {
					 fromYear: document.getElementById('fromYear-add').value,
					 fromMonth: document.getElementById('fromMonth-add').value,
					 toMonth: document.getElementById('toMonth-add').value,
					 toYear: document.getElementById('toYear-add').value,
					 position: document.getElementById('position-add').value,
					 pride: document.getElementById('pride-add').value,
					 name: document.getElementById('name-add').value,
					 sphere: document.getElementById('sphere-add').value,
					 city: document.getElementById('city-add').value,
					 site: document.getElementById('site-add').value,
					 jobs: document.getElementById('jobs-add').value,
					 achivki: document.getElementById('achivki-add').value
			 	};
			lst.push(obj);
			this.get('instance').push('work', JSON.stringify(lst));
			this.get('instance').push('wrkList', 'need');
			this.get('instance').push('step3', true);
			this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		}
	}
});
