import Ember from 'ember';

export default Ember.Component.extend({
	language: 'ru',
	isOpen: false,

	actions: {
		toggleModal() {
			if(this.get('isOpen') === false) {
				this.set('isOpen', true);
			} else {
				this.set('isOpen', false);
			}
		},
		chooseEnglish() {
			this.set('language', 'eng');
			if(this.get('isOpen') === false) {
				this.set('isOpen', true);
			} else {
				this.set('isOpen', false);
			}
		},
		chooseRussian() {
			this.set('language', 'ru');
			if(this.get('isOpen') === false) {
				this.set('isOpen', true);
			} else {
				this.set('isOpen', false);
			}
		}
	}
});
