import Ember from 'ember';

export default Ember.Component.extend({
	isOpen: false,

	instance: Ember.inject.service('app-instance'),

	fio: null,
	positopn: null,
	wage: null,
	phone: null,
	skype: null,
	email: null,
	site: null,
	dateOfBirth: null,
	city: null,
	gender: null,
	citizenship: null,
	komandirovki: null,
	pereezd: null,
	cityList: [
		'Тюмень',
		'Екатеринбург',
		'Москва'
	],
	citizenshipList: [
		'российское',
		'американское'
	],
	isRussian: false,
	isUSA: false,
	isTyumen: false,
	isEkb: false,
	isMoscow: false,

	init() {
	    this._super(...arguments);
	    [
	    'gender',
	    'city',
	    'citizenship',
	    'site',
	    'fio',
	    'position',
	    'wage',
	    'phone',
	    'skype',
	    'email',
	    'dateOfBirth',
	    'pereezd',
	    'komandirovki'].forEach((item) => {
	    	this.set(item, this.get('instance').pop(item));
	    });
	},

	actions: {
		citizenshipSearch() {
			let tmp = null,
				templateString = null,
				index = 0;
			this.set('isRussian', false);
			this.set('isUSA', false);
			this.get('citizenshipList').forEach((citizenship) => {
				templateString = citizenship.toUpperCase();
				tmp = document.getElementById('citizenship').value.toUpperCase();
				if(templateString.search(tmp) !== -1) {
					if(index === 0) {
						this.set('isRussian', true);
					} 
					if(index === 1) {
						this.set('isUSA', true);
					}
				}
				index +=1;
			});
		},
		chooseCitizenship(citizenship) {
			this.set('isRussian', false);
			this.set('isUSA', false);
			document.getElementById('citizenship').value = citizenship;
		},
		citySearch() {
			let tmp = null,
				templateString = null,
				index = 0;
			this.set('isTyumen', false);
			this.set('isEkb', false);
			this.set('isMoscow', false);
			this.get('cityList').forEach((city) => {
				templateString = city.toUpperCase();
				tmp = document.getElementById('city').value.toUpperCase();
				if(templateString.search(tmp) !== -1) {
					if(index === 0) {
						this.set('isTyumen', true);
					}
					if(index === 1) {
						this.set('isEkb', true);
					}
					if(index === 2) {
						this.set('isMoscow', true);
					}
				}
				index +=1;
			});
		},
		chooseCity(city) {
			this.set('isTyumen', false);
			this.set('isEkb', false);
			this.set('isMoscow', false);
			document.getElementById('city').value = city;
		},
		toggleModal() {
			if(this.get('isOpen') === false) {
				this.set('isOpen', true);
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
			} else {
				this.set('isOpen', false);
				document.getElementsByTagName('html')[0].style.overflow = "";
			}
		},
		setMale() {
			this.set('gender', 'male');
			this.get('instance').set('gender', this.get('gender'));
		},
		setFemale() {
			this.set('gender', 'female');
			this.get('instance').push('gender', this.get('gender'));
		},
		saveInfo() {
			[
			'city',
			'citizenship',
			'fio',
			'dateOfBirth',
			'position',
			'wage',
			'skype',
			'email',
			'phone',
			'site'].forEach((item) => {
		    	this.get('instance').push(item, window.document.getElementById(item).value || 'Не указано');
		    });
		    window.localStorage.setItem('pereezd', window.document.getElementById('pereezd').value);
		    window.localStorage.setItem('komandirovki', window.document.getElementById('komandirovki').value);
		    this.get('instance').push('step1', true);
		    this.set('isOpen', false);
			document.getElementsByTagName('html')[0].style.overflow = "";
		}
	}
});
