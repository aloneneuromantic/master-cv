import Ember from 'ember';
import imageCropper from 'ember-cli-image-cropper/components/image-cropper';

export default imageCropper.extend({
    instance: Ember.inject.service('app-instance'),
    //override default options of cropper
    aspectRatio: 1,
    minCropBoxWidth: 100,
    minCropBoxHeight: 100,
    cropperContainer: '.cropper-container > img',
    previewClass: '.img-preview',
    croppedAvatar: null,
    avatar: null,
    isOpen: false,
    isShow: true,
    language: null,
    template: null,
    timer: null,
    fileAvatar: null,

    willRender() {
        this.set('avatar', this.get('instance').pop('avatar'));
        this.set('language', this.get('instance').pop('language'));
        this.set('template', this.get('instance').pop('template'));

        if(this.get('timer') === null) {
            this.set('timer', setInterval(()=> {
                if(this.get('language') !== this.get('instance').pop('language')) {
                    this.set('language', this.get('instance').pop('language'));
                }

                if(this.get('template') !== this.get('instance').pop('template')) {
                    this.set('template', this.get('instance').pop('template'));
                }
            }, 100));
        }
    },


    willDestroyElement() {
        window.clearInterval(this.get('timer'));
    },


    actions:
    {
        openModal() {
            this.set('isOpen', true);
            if(window.localStorage.getItem('file') !== 'none') {
                this.set('fileAvatar', window.localStorage.getItem('file'));
            }
        },
        closeModal() {
            this.set('isOpen', false);
        },
        uploadImage() {
            document.getElementById('imgInp').click();
            let tmp = setInterval(() => {
                if(this.get('isOpen') ===  true) {
                    if(document.getElementById('imgInp').files[0] !== undefined) {
                        let src = window.URL.createObjectURL(document.getElementById('imgInp').files[0]),
                            image = document.getElementById('imageToCropp');
                        window.localStorage.setItem('file', src);
                        this.set('fileAvatar', src);
                        image.src = src;
                        document.getElementsByClassName('cropper-canvas')[0].childNodes[0].src = src;
                        document.getElementsByClassName('cropper-view-box')[0].childNodes[0].src = src;
                        window.clearInterval(tmp);
                    } 
                }
            }, 100);
        },
        getCroppedAvatar() {
            let container = this.$(this.get('cropperContainer')),
                croppedImage = container.cropper('getCroppedCanvas');
            this.get('instance').push('avatar', croppedImage.toDataURL());
            this.set('isOpen', false);
        }
    }
});
