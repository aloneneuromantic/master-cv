import Ember from 'ember';

export default Ember.Component.extend({
	instance: Ember.inject.service('app-instance'),

	tagName: 'section',
	classNames: ['hero'],
	language: null,
	timer: null,

	willRender() {
		this.set('language', this.get('instance').pop('language'));

		if(this.get('timer') === null) {
			this.set('timer', setInterval(() => {
				if(this.get('language') !== this.get('instance').pop('language')) {
					this.set('language', this.get('instance').pop('language'));				
				}
			}, 100));
		}
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	attributeBindings: [
		'title', 
		'size' , 
		'color', 
		'text-align', 
		'title', 
		'subtitle', 
		'state',
		'linkTwiiter',	
		'linkVK',
		'linkFB',
		'linkIn',
		'linkGP'
		],
	classNameBindings: [
		'size', 
		'color'
		]
});
