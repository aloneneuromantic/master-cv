import Ember from 'ember';

export default Ember.Component.extend({
	isOpen: false,

	actions: {
		openImage() {
			this.set('isOpen', true);
		},
		closeImage() {
			this.set('isOpen', false);
		}
	}
});
