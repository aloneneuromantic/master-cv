import Ember from 'ember';

export default Ember.Component.extend({

	instance: Ember.inject.service('app-instance'),

	modalState: null,
	language: null,
	template: null,
	timer: null,
	isSvid: true,
	isSert: true,
	step: null,

	attributeBindings: [
        'description',
        'type',
        'name',
        'dateYear',
        'dateMonth'
    ],

	willRender() {
		this.set('language', this.get('instance').pop('language'));
		this.set('template', this.get('instance').pop('template'));
		this.set('step', this.get('instance').pop('currentStep'));
		this.set('timer', setInterval(() => {
			if(this.get('language') !== this.get('instance').pop('language')) {
				this.set('language', this.get('instance').pop('language'));
			}
			if(this.get('template') !== this.get('instance').pop('template')) {
				this.set('template', this.get('instance').pop('template'));
			}
		}, 100));
	},

	willDestroyElement() {
		window.clearInterval(this.get('timer'));
	},

	actions: {
		deleteWork() {
			let lst = JSON.parse(this.get('instance').pop('additionalKnowledge')),
				tmp = [],
			 	max = lst.length,
			 	iterator = 1;
			lst.forEach((place) => {
			 	if(place.description !== this.get('description')) {
			 		tmp.push(place);
			 	}

			 	if(iterator === max) {
			 		this.get('instance').push('additionalKnowledge', JSON.stringify(tmp));
			 		this.get('instance').push('addEdList', 'need');
					document.getElementById(`trashModal-${this.get('name')}`).classList.add('is-active');
			 	} else {
			 		iterator += 1;
			 	}
			});
		},
		openEdit() {
			document.getElementById(`editModal-${this.get('name')}`).classList.add('is-active');
			document.getElementsByTagName('html')[0].style.overflow = "hidden";
		},
		saveEdit() {
			let tmp = JSON.parse(this.get('instance').pop('additionalKnowledge'));
			tmp.forEach((item) => {
				if(item.description === this.get('description')){
					item.name = document.getElementById(`name-${this.get('name')}`).value;
					item.dateYear = document.getElementById(`dateYear-${this.get('name')}`).value;
					item.dateMonth = document.getElementById(`dateMonth-${this.get('name')}`).value;
					item.description = document.getElementById(`description-${this.get('name')}`).value;
					if(document.getElementById(`type-${this.get('name')}-0`).checked === true) {
						item.type = 'сертефикат';
					} else {
						item.type = 'свидетельство';
					}
					this.set('type', item.type);
					this.set('description', item.desctiption);
					this.set('name', item.name);
					this.set('dateMonth', item.dateMonth);
					this.set('dateYear', item.dateYear);
					this.get('instance').push('additionalKnowledge', JSON.stringify(tmp));
					document.getElementById(`editModal-${this.get('name')}`).classList.remove('is-active');
					this.get('instance').push('addEdList', 'need');
					document.getElementsByTagName('html')[0].style.overflow = "";
				}
			});
		},
		closeEdit() {
			document.getElementById(`editModal-${this.get('name')}`).classList.remove('is-active');
			document.getElementsByTagName('html')[0].style.overflow = "";
		},
		openTrash() {
			document.getElementById(`trashModal-${this.get('name')}`).classList.add('is-active');
		},
		closeTrash() {
			document.getElementById(`trashModal-${this.get('name')}`).classList.remove('is-active');
		}
	}
});
