import Ember from 'ember';

export default Ember.Service.extend({
	localStorage: null,

	init() {
	    this._super(...arguments);
	    this.set('localStorage', window.localStorage);
	    if(this.get('localStorage').length === 1) {
	    	this.get('localStorage').setItem('fio', 'Анонимный Пользователь');
	    	this.get('localStorage').setItem('position', 'Неизвестно');
	    	this.get('localStorage').setItem('template', 'one');
	    	this.get('localStorage').setItem('wage', '0');
	    	this.get('localStorage').setItem('pereezd', '-1');
	    	this.get('localStorage').setItem('komandirovki', '-1');
	    	this.get('localStorage').setItem('phone', '8 (000) 000 00 00');
	    	this.get('localStorage').setItem('skype', 'anon_user');
	    	this.get('localStorage').setItem('email', 'example@gmail.com');
	    	this.get('localStorage').setItem('site', 'emberjs.com');
	    	this.get('localStorage').setItem('dateOfBirth', '15.02.1988');
	    	this.get('localStorage').setItem('needRefresh', 'none');
	    	this.get('localStorage').setItem('openedModal', 'editWork');
	    	this.get('localStorage').setItem('edList', 'none');
			this.get('localStorage').setItem('addEdList', 'none');
	    	this.get('localStorage').setItem('wrkList', 'none');
	    	this.get('localStorage').setItem('gender', 'male');
	    	this.get('localStorage').setItem('city', 'Тюмень');
	    	this.get('localStorage').setItem('citizenship', 'российское');
			this.get('localStorage').setItem('languageRefresh', 'none');
	    	let tmp = [{
	    					fromYear: '0',
	    					fromMonth: '0',
	    					toYear: 'current',
	    					toMonth: 'current',
	    					position: 'Подниматель пингвинов',
							pride: '0',
	    					name: 'ООО "Урал-Сервис" (оптовая торговля строительными материалами)',
							sphere: 'чудеса',
	    					city: 'Тюмень',
	    					site: 'example.ru',
	    					jobs: 'один\nдва\nтри',
	    					achivki: 'один\nдва'
	    				}];
	    	this.get('localStorage').setItem('work', JSON.stringify(tmp));
	    	tmp = [{
				    		fromYear: '2016',
	    					fromMonth: '01',
	    					toYear: '2017',
	    					toMonth: "01",
	    					department: 'Факультет всеобщего счастья',
	    					formEdk: 'Очная',
				    		from: '1988',
				    		to: '2001',
				    		position: 'среднее оконченное',
				    		name: 'Тюменский государственный университет',
				    		city: 'Тюмень',
				    		site: 'example.tu',
				    		comments: 'Комментарии'
	    	}];
	    	this.get('localStorage').setItem('knowledge', JSON.stringify(tmp));
			tmp = [{
						description: 'Допонительное образование',
						type: 'сертефикат',
						name: 'НИИ ЧАВО',
						dateYear: '2017',
						dateMonth: '01'
				}];
			this.get('localStorage').setItem('additionalKnowledge', JSON.stringify(tmp));
	    	tmp = {
	    		baseLanguage: 'русский',
	    		otherLanguages: [
					{
						name: 'русский',
						state: '1'
					},
				],
	    		driverLicense: 'A',
	    		computer: 'Программист',
	    		personalQualities: 'Спокойно поднимаю пингвинов',
	    		additionalInfo: 'Нет',
					prichina: 'Нет',
					save: 'no',
					profskils: 'Нет'
	    	};
	    	this.get('localStorage').setItem('other', JSON.stringify(tmp));
	    	this.get('localStorage').setItem('step1', false);
	    	this.get('localStorage').setItem('step2', false);
	    	this.get('localStorage').setItem('step3', false);
	    	this.get('localStorage').setItem('step4', false);
	    	this.get('localStorage').setItem('language', 'ru');
	    	this.get('localStorage').setItem('avatar', '/photo-icon.png');

	    }
	},

	push(name, value) {
		this.get('localStorage').setItem(name, value);
	},
	pop(name) {
		return this.get('localStorage').getItem(name);
	},


	getData() {
		return {
			language: this.get('localStorage').getItem('language'),
			fio: this.get('localStorage').getItem('fio'),
	    	position: this.get('localStorage').getItem('position'),
	    	wage: this.get('localStorage').getItem('wage'),
			pereezd: this.get('localStorage').getItem('pereezd'),
			komandirovki: this.get('komandirovki').getItem('komandirovki'),
	    	phone: this.get('localStorage').getItem('phone'),
	    	skype: this.get('localStorage').getItem('skype'),
	    	email: this.get('localStorage').getItem('email'),
	    	site: this.get('localStorage').getItem('site'),
	    	dateOfBirth: this.get('localStorage').getItem('dateOfBirth'),
			gender: this.get('localStorage').getItem('gender'),
			city: this.get('localStorage').getItem('city'),
	    	avatar: this.get('localStorage').getItem('avatar'),
			citizenship: this.get('localStorage').getItem('citizenship'),
	    	work: this.get('localStorage').getItem('work'),
	    	knowledge: this.get('localStorage').getItem('knowledge'),
			additionalKnowledge: this.get('localStorage').getItem('additionalKnowledge'),
	    	other: this.get('localStorage').getItem('other'),
		};
	}
});
