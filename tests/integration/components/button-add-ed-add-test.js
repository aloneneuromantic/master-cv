import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('button-add-ed-add', 'Integration | Component | button add ed add', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{button-add-ed-add}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#button-add-ed-add}}
      template block text
    {{/button-add-ed-add}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
